package com.example.codeslave.chatapplication.activity.URL;

public class URLConstants {

    public static String BASE_URL = "http://adarekar.pythonanywhere.com/";

    public static String LOGIN_URL = "/rest-auth/login/";

    public static String SIGN_UP_URL = "/rest-auth/registration/";

    public static String GET_PROFILE_URL = "/rest-auth/";
}
